from zcrmsdk import ZCRMRestClient, ZohoOAuth

ZCRMRestClient.initialize()
oauth_client = ZohoOAuth.get_client_instance()
refresh_token = "Your_Refresh_Token"
user_identifier = "Your_Zoho_Account_Email"
oauth_tokens = oauth_client.generate_access_token_from_refresh_token(refresh_token, user_identifier)
