# -*- coding: utf-8 -*-

import socket

import sys

from zcrmsdk import ZCRMRestClient, ZCRMException, ZCRMModule

from collections import defaultdict

from email.mime.text import MIMEText

import re

import smtplib

import ssl

'''
Preparing the default dictionary of the default dictionary to have salesperson as the key 
and value as another dictionary where 'lead status' is key and list of contacts as values

'''

list_of_leads = []

leads = defaultdict(lambda: defaultdict(list))

leads.clear()

ZCRMRestClient.initialize()

page = 0

'''
Start iterating over all the pages and all the Leads records of our Zoho CRM 
account retrieving all the necessary fields contents and building our default dictionary of default dictionary
which we will going to use down below in the code.
'''

while True:
    try:
        page += 1
        module_ins = ZCRMModule.get_instance('Leads')  # Module API Name
        resp = module_ins.get_records(None, None, None, page, 100)  # Start index and end index
        resp_info = resp.response_json['info']  # getting response info in json format
        record_ins_arr = resp.data  # list of ZCRMRecord instance
        for record_ins in record_ins_arr:
            product_data = record_ins.field_data
            if product_data.get("Sales_Person") is not None:
                sales_rep = str(product_data.get("Sales_Person").lower())
                client = str(product_data.get("Client"))
                zip_code = str(product_data.get("Zip_Code"))
                city = str(product_data.get("City"))
                tel_number = str(product_data.get("Phone"))
                sales_rep_date = str(product_data.get("Contact_Date"))
                state = str(product_data.get("State"))
                lead_status = str(product_data.get("Lead_Status1"))
                leads[sales_rep][lead_status].append([client, zip_code, city, tel_number, state, sales_rep_date])
        more_pages = resp.response_json['info']['more_records']
        if not more_pages:
            break
        else:
            continue

    except ZCRMException as ex:
        print(ex.status_code)
        print(ex.error_message)
        print(ex.error_code)
        print(ex.error_details)
        print(ex.error_content)

'''
Read the file containing the salespersons' emails in format name.surname@somemail.com
into the list and creating a list for salespersons'  surnames. Then putting them all together 
into the dictionary where key is the surname of the sales person and value is his email
'''

with open('rep_email_adress.txt') as f:
    email_list = f.read().splitlines()

sales_rep_email = dict()

sales_rep_email.clear()

sales_rep_name_list = []

for k in leads.keys():
    sales_rep_name_list.append(k.lower())

for name in sales_rep_name_list:
    for email in email_list:
        if re.search(r'\b' + re.escape(name.lower()) + r'\b', email):
            sales_rep_email[name.lower()] = email

'''
Check if some salesperson email was missing in the email input file 
And if so, write a message into another file pointing to the surname of the salesperson which email is missing in the email input file. So you can add missing
sales person mail into the email input file next time you run the script.
'''

for rep in sales_rep_name_list:
    if rep.lower() not in sales_rep_email.keys():
        with open('check_if_sales_rep_mail_exists.txt', 'a') as f:
            f.write('Sales representative  with surname ' + rep.capitalize() + ' does not have email yet. '
                                                                               'Please add it next time script runs' + '\n')
        f.close()

'''
Finally, we iterate over our Leads default dictionary of default dictionaries and dictionary where we hold our salesperson surname and email key and value and building the
Messages which are grouped by Leads status and for each of them we are sending out the email of the salesperson they belong to. 
In this case, I'm using the Gmail SMTP server. But you are free to use any other mail server.
'''

for k, lead in leads.items():

    if sales_rep_email.get(k):

        email_addr = sales_rep_email.get(k)

        for status, contacts in lead.items():

            print("")

            lead_status_msg = ("Lead status " + f'"{status}"' + ': ' + "\n")

            lead_status_columns_msg = ("Client " + "; " + "Zip code" + "; " + "City" + "; " + "Phone" + "; " + "State" +
                                       "; " + "Contact Date" + "; " + "\n")

            lead_status_body = '\n'.join('; '.join(column) for column in contacts)

            msg = MIMEText((lead_status_msg + "\n" + lead_status_columns_msg + lead_status_body), 'plain', 'utf-8')

            msg['To'] = email_addr.encode('idna').decode("utf-8")

            msg['From'] = 'name.surname@somemail.com'

            msg['Subject'] = 'Your Leads with status ' + "\"" + status + "\""

# Sending out the email messages to Gmail inbox using SSL connection. Please comment on this code if you want
# to test and debug mails sending firs using local SMTPd server which are commented down bellow this part of code.

            port = 465  # For SSL

            # Provide here the email account password that you are going to use to
            # send out the messages with leads status reports to the salespersons'
            password = ''

            # Create a secure SSL context
            context = ssl.create_default_context()

            try:
                with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
                    server.login("name.surname@gmail.com", password)
                    server.sendmail(msg['From'], msg['To'], msg.as_string())
            except socket.error as e:
                print ("Could not connect to SMTP server")
            except:
                print("Unknown error:", sys.exc_info()[0])

# Uncomment this part of code below and run command -> python smtpd_debug.py in terminal to start local SMTPd server
# which will debug mail sending and show you output result in terminal instead of sending real mails out to Gmail

            '''
            server = smtplib.SMTP('127.0.0.1', 1025) # Only for smtpd debug server

            server.set_debuglevel(True)  # show communication with the server

            try:
                server.sendmail(msg['From'], msg['To'], msg.as_string())
            finally:
                server.quit()
            '''
