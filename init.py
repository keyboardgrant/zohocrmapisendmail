from zcrmsdk import ZCRMRestClient, ZohoOAuth

ZCRMRestClient.initialize()

oauth_client = ZohoOAuth.get_client_instance()

grand_token = "Your_Grant_Token_Here"

oauth_tokens = oauth_client.generate_access_token(grand_token)

print(oauth_tokens.refreshToken)

